#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define TAM 200

int main() 
{
	//Borrar la tubería en la consola: rm /tmp/TuberiaLogger

	int mk=mkfifo("/tmp/TuberiaLogger",0777);
	if(mk==-1){
		perror("Fallo al crear la tuberia");
		exit(1);
		}

 	int pipe=open("/tmp/TuberiaLogger",O_RDONLY);
 
 	while(1){
 		char cad[TAM];	
		int lectura=read(pipe, cad, sizeof(cad));

		if(lectura==-1){
			perror("Fallo al leer la tuberia");
			exit(1);
		}

		if(lectura==0)
			break;
		
 		printf("%s\n", cad);
 	}
 
	int cerrar=close(pipe);
		if(cerrar==-1){
			perror("Fallo al cerrar la tuberia");
			exit(1);
		}

 	unlink("/tmp/TuberiaLogger");
 
 	return 0;
}

