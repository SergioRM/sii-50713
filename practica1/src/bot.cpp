#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"

int main()
{
	//Cerrar el archivo en la consola: rm /tmp/datosComp.txt

	int file;
	DatosMemCompartida* pMemC;
	char* org;

	file=open("/tmp/datosComp.txt",O_RDWR);

	org=(char*)mmap(NULL,sizeof(*(pMemC)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	close(file);
	
	pMemC=(DatosMemCompartida*)org;


	while(1)
	{
		float posicion_raqueta;

		posicion_raqueta=(pMemC->raqueta1.y2+pMemC->raqueta1.y1)/2;
		if(posicion_raqueta<pMemC->esfera.centro.y)
			pMemC->accion=1;
		else if(posicion_raqueta>pMemC->esfera.centro.y)
			pMemC->accion=-1;
		else
			pMemC->accion=0;
		usleep(25000);
	}

	munmap(org,sizeof(*(pMemC)));
	
	return 0;
}
